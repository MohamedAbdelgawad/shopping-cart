package Main;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import SQLPresistenceTesting.SQLPersistenceTest;
import persistenceLayer.SQLPersistence;
import shoppingCart.ShoppingCartTest;

public class Main {

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(SQLPersistenceTest.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
	}

}
